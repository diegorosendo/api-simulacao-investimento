package br.com.investimento.investimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulacaoInvestimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulacaoInvestimentoApplication.class, args);
	}

}
