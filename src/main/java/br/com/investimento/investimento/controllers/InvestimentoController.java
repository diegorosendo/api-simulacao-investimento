package br.com.investimento.investimento.controllers;

import br.com.investimento.investimento.models.Investimento;
import br.com.investimento.investimento.models.Simulacao;
import br.com.investimento.investimento.models.dtos.SimulacaoCriarRespostaDTO;
import br.com.investimento.investimento.services.InvestimentoService;
import br.com.investimento.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;


    @PostMapping
    public Investimento registrarInvestimento(@RequestBody Investimento investimento){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return investimentoObjeto;
    }

    @GetMapping
    public Iterable<Investimento> buscarTodosOsInvestimentos(){
        Iterable<Investimento> produtos = investimentoService.buscarTodosOsInvestimentos();
        return produtos;
    }

    @PostMapping("/{idProduto}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoCriarRespostaDTO realizarSimulacao(@PathVariable(name = "idProduto") int idProduto, @RequestBody @Valid Simulacao simulacao){
        try{
            SimulacaoCriarRespostaDTO simulacaoCriarRespostaDTO = simulacaoService.realizarSimulacao(idProduto, simulacao);
            return simulacaoCriarRespostaDTO;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
