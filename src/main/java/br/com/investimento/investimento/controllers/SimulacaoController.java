package br.com.investimento.investimento.controllers;

import br.com.investimento.investimento.models.Simulacao;
import br.com.investimento.investimento.models.dtos.SimulacaoConsultarRespostaDTO;
import br.com.investimento.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {


    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> buscarTodasAsSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoService.buscarTodasAsSimulacoes();
        return simulacoes;
    }


    @GetMapping("/{id}")
    public SimulacaoConsultarRespostaDTO consultarSimulacaoPorId(@PathVariable(name = "id") long id){
        try{
            SimulacaoConsultarRespostaDTO simulacaoConsultarRespostaDTO = simulacaoService.consultarSimulacaoPorId(id);
            return simulacaoConsultarRespostaDTO;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
