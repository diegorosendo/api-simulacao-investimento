package br.com.investimento.investimento.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id;

    @Column(unique = true)
    private String nome;
    private double rendimentoAoMes;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Simulacao> simulacoes;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

}
