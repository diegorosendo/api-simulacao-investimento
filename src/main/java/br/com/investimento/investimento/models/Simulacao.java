package br.com.investimento.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long id;

    private String nomeInteressado;


    @Email(message = "O formato do email é invalido")
    @NotNull(message = "Email não pode ser nulo")
    @NotBlank(message = "Email deve ser informado")
    private String email;
    private double valorAplicado;
    private int quantidadeMeses;


    @ManyToOne (cascade = CascadeType.ALL)
    private Investimento investimento;

    public Simulacao() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        BigDecimal bd = new BigDecimal(this.valorAplicado).setScale(2, RoundingMode.FLOOR);
        return  bd.doubleValue();
    }

    public void setValorAplicado(Double valorAplicado) {
        BigDecimal bd = new BigDecimal(valorAplicado).setScale(2, RoundingMode.FLOOR);
        this.valorAplicado = bd.doubleValue();
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
