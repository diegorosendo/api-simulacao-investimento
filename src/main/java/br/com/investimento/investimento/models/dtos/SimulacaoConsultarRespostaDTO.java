package br.com.investimento.investimento.models.dtos;

import br.com.investimento.investimento.models.Simulacao;

public class SimulacaoConsultarRespostaDTO {
    private Simulacao simulacao;
    private SimulacaoCriarRespostaDTO simulacaoCriarRespostaDTO;

    public SimulacaoConsultarRespostaDTO() {
    }

    public Simulacao getSimulacao() {
        return simulacao;
    }

    public void setSimulacao(Simulacao simulacao) {
        this.simulacao = simulacao;
    }

    public SimulacaoCriarRespostaDTO getSimulacaoCriarRespostaDTO() {
        return simulacaoCriarRespostaDTO;
    }

    public void setSimulacaoCriarRespostaDTO(SimulacaoCriarRespostaDTO simulacaoCriarRespostaDTO) {
        this.simulacaoCriarRespostaDTO = simulacaoCriarRespostaDTO;
    }
}
