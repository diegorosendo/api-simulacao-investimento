package br.com.investimento.investimento.models.dtos;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SimulacaoCriarRespostaDTO {

    private double rendimentoPorMes;
    private double montante;

    public SimulacaoCriarRespostaDTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(Double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }


}
