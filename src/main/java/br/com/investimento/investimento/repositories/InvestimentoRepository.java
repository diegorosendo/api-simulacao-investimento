package br.com.investimento.investimento.repositories;

import br.com.investimento.investimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository <Investimento, Integer> {
}
