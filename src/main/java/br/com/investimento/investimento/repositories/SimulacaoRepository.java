package br.com.investimento.investimento.repositories;

import br.com.investimento.investimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository <Simulacao, Long> {
}
