package br.com.investimento.investimento.services;

import br.com.investimento.investimento.models.Investimento;
import br.com.investimento.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarTodosOsInvestimentos(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Investimento buscarInvestimentoPorId(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (optionalInvestimento.isPresent()){

            return optionalInvestimento.get();
        }
        throw new RuntimeException("O investimento não foi encontrado");
    }
}


