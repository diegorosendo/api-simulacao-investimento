package br.com.investimento.investimento.services;

import br.com.investimento.investimento.models.Investimento;
import br.com.investimento.investimento.models.Simulacao;
import br.com.investimento.investimento.models.dtos.SimulacaoConsultarRespostaDTO;
import br.com.investimento.investimento.models.dtos.SimulacaoCriarRespostaDTO;
import br.com.investimento.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    public SimulacaoCriarRespostaDTO realizarSimulacao(int idInvestimento, Simulacao simulacao){

        //buscar Investimento
        Investimento investimento = new Investimento();
        investimento = investimentoService.buscarInvestimentoPorId(idInvestimento);
        simulacao.setInvestimento(investimento);

        //criar Simulação
        Simulacao simulacaoObjeto =simulacaoRepository.save(simulacao);

        //popular RespostaDTO
        SimulacaoCriarRespostaDTO simulacaoCriarRespostaDTO = new SimulacaoCriarRespostaDTO();

        simulacaoCriarRespostaDTO = this.calcularSimulacao(simulacaoObjeto);


        return simulacaoCriarRespostaDTO;
    }

    private SimulacaoCriarRespostaDTO calcularSimulacao(Simulacao simulacao){

        SimulacaoCriarRespostaDTO simulacaoCriarRespostaDTO = new SimulacaoCriarRespostaDTO();

        double capitalAcumulado = simulacao.getValorAplicado();
        for (int i=1; i <= simulacao.getQuantidadeMeses(); i++){
            capitalAcumulado += capitalAcumulado * simulacao.getInvestimento().getRendimentoAoMes();
        }

        simulacaoCriarRespostaDTO.setMontante(capitalAcumulado);
        simulacaoCriarRespostaDTO.setRendimentoPorMes(simulacao.getInvestimento().getRendimentoAoMes());

        return simulacaoCriarRespostaDTO;
    }

    public Iterable<Simulacao> buscarTodasAsSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }

    public SimulacaoConsultarRespostaDTO consultarSimulacaoPorId(long id){


        Simulacao simulacao = new Simulacao();
        Optional<Simulacao> optionalSimulacao= simulacaoRepository.findById(id);
        if (optionalSimulacao.isPresent()){
            simulacao = optionalSimulacao.get();
        }else{
            throw new RuntimeException("A simulação não foi encontrada");
        }

        //popular RespostaDTO
        SimulacaoConsultarRespostaDTO simulacaoConsultarRespostaDTO = new SimulacaoConsultarRespostaDTO();
        simulacaoConsultarRespostaDTO.setSimulacao(simulacao);
        simulacaoConsultarRespostaDTO.setSimulacaoCriarRespostaDTO(this.calcularSimulacao(simulacao));
//        simulacaoConsultarRespostaDTO.getSimulacaoCriarRespostaDTO().setRendimentoPorMes(simulacao.getInvestimento().getRendimentoAoMes());

        return simulacaoConsultarRespostaDTO;

    }

}
